/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jsf2crud.util;

import javax.imageio.spi.ServiceRegistry;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.DefaultComponentSafeNamingStrategy;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author nleit_000
 */
public class HibernateUtil  {
	
	private static final SessionFactory sessionFactory;
	
	static {
	
	try {
	
	sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
	
	} catch (Throwable ex) {
	
	System.err.println("Initial SessionFactory creation failed." + ex);
	
	throw new ExceptionInInitializerError(ex);
	
	}
	
	}
	
	public static SessionFactory getSessionFactory() {
	
	return sessionFactory;
	
	}

	
	

}