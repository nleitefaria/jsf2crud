package com.mycompany.jsf2crud.dao;

import java.util.List;

import com.mycompany.jsf2crud.entity.Feature;

public interface FeatureDAO 
{
	List<Feature> findAll();

}
