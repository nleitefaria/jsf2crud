package com.mycompany.jsf2crud.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.mycompany.jsf2crud.dao.CustomerDAO;
import com.mycompany.jsf2crud.entity.Customer;
import com.mycompany.jsf2crud.util.HibernateUtil;

public class CustomerDAOImpl implements CustomerDAO
{	
	public CustomerDAOImpl() 
	{
	}
	
	@SuppressWarnings("unchecked")
	public List<Customer> findAll() 
    {
		Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
        List<Customer> customerList = null;
        try 
        {
        	tx = session.beginTransaction();
            Query q = session.createQuery ("from Customer as customer");
            customerList = (List<Customer>) q.list();            
        } 
        catch (RuntimeException e) 
        {
            if (tx != null) 
            {
                tx.rollback();
            }
            e.printStackTrace();
        } 
        finally 
        {
            session.close();
        }       
        return customerList;
    }
    
    public Customer findOne(String customerID) 
    {   
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
        Customer customer = null;
        try 
        { 
        	tx = session.beginTransaction();            
            Query q = session.createQuery ("from Customer as customer left join fetch customer.payments where customer.id=" + customerID);
            customer = (Customer) q.list().get(0);            
        } 
        catch (RuntimeException e) 
        {
            if (tx != null) 
            {
                tx.rollback();
            }
            e.printStackTrace();
        } 
        finally 
        {
            session.close();
        }
       
        return customer;
    }
    
    public void save(Customer customer)
    {
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
        try 
        {
        	tx = session.beginTransaction();
            session.save(customer);
            session.getTransaction().commit();
        } 
        catch (RuntimeException e) 
        {
            if (tx != null) 
            {
                tx.rollback();
            }
            e.printStackTrace();
        } 
        finally 
        {
            session.flush();
            session.close();
        }
    }
      
    public void delete(Integer id)
    {
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
    
    	try
    	{
    		tx = session.beginTransaction();
    		Customer student = (Customer)session.get(Customer.class, id); 
    		session.delete(student); 
    		tx.commit();
    	}
    	catch (HibernateException e) 
    	{
    		if (tx!=null) tx.rollback();
    		e.printStackTrace(); 
    	}
    	finally 
    	{
    		session.close(); 
    	}
    }
}
