package com.mycompany.jsf2crud.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mycompany.jsf2crud.dao.PaymentDAO;
import com.mycompany.jsf2crud.entity.Customer;
import com.mycompany.jsf2crud.entity.Payment;
import com.mycompany.jsf2crud.util.HibernateUtil;

public class PaymentDAOImpl implements PaymentDAO
{
	public PaymentDAOImpl() 
	{
	}
	
	public List<Payment> findAll() 
    {
		Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
        List<Payment> paymentList = null;
        try 
        {
        	tx = session.beginTransaction();
            Query q = session.createQuery ("from Payment as payment left join fetch payment.customer");
            paymentList = (List<Payment>) q.list();            
        } 
        catch (RuntimeException e) 
        {
            if (tx != null) 
            {
                tx.rollback();
            }
            e.printStackTrace();
        } 
        finally 
        {
            session.close();
        }       
        return paymentList;
    }

	public Payment findOne(String paymentID) 
    {
		Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
    	Payment payment = null;
        try 
        {
        	tx = session.beginTransaction();
            Query q = session.createQuery ("from Payment as payment left join fetch payment.customer where payment.id=" + paymentID);
            payment = (Payment) q.list().get(0);            
        } 
        catch (RuntimeException e) 
        {
            if (tx != null) 
            {
                tx.rollback();
            }
            e.printStackTrace();
        } 
        finally 
        {
            session.close();
        }       
        return payment;
    }

}
