package com.mycompany.jsf2crud.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mycompany.jsf2crud.dao.FeatureDAO;
import com.mycompany.jsf2crud.entity.Feature;
import com.mycompany.jsf2crud.util.HibernateUtil;

public class FeatureDAOImpl implements FeatureDAO{
	
	public FeatureDAOImpl() 
	{
	}
	
	@SuppressWarnings("unchecked")
	public List<Feature> findAll() 
    {
		Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
        List<Feature> featureList = null;
        try 
        {
        	tx = session.beginTransaction();
            Query q = session.createQuery ("from Feature as feature");
            featureList = (List<Feature>) q.list();            
        } 
        catch (RuntimeException e) 
        {
            if (tx != null) 
            {
                tx.rollback();
            }
            e.printStackTrace();
        } 
        finally 
        {
            session.close();
        }       
        return featureList;
    }

}
