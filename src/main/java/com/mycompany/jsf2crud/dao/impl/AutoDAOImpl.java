package com.mycompany.jsf2crud.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.mycompany.jsf2crud.dao.AutoDAO;
import com.mycompany.jsf2crud.entity.Auto;
import com.mycompany.jsf2crud.entity.Customer;
import com.mycompany.jsf2crud.util.HibernateUtil;

public class AutoDAOImpl implements AutoDAO
{
	public AutoDAOImpl() 
	{
	}
	
	@SuppressWarnings("unchecked")
	public List<Auto> findAll() 
    {
		Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
        List<Auto> autoList = null;
        try 
        {
        	tx = session.beginTransaction();
            Query q = session.createQuery ("from Auto as auto");
            autoList = (List<Auto>) q.list();            
        } 
        catch (RuntimeException e) 
        {
            if (tx != null) 
            {
                tx.rollback();
            }
            e.printStackTrace();
        } 
        finally 
        {
            session.close();
        }       
        return autoList;
    }
	
	public Auto findOne(String autoID) 
    {   
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	org.hibernate.Transaction tx = null;
    	Auto auto = null;
        try 
        { 
        	tx = session.beginTransaction();            
            Query q = session.createQuery ("from Auto as auto left join fetch auto.autoFeatures where auto.id=" + autoID);
            auto = (Auto) q.list().get(0);            
        } 
        catch (RuntimeException e) 
        {
            if (tx != null) 
            {
                tx.rollback();
            }
            e.printStackTrace();
        } 
        finally 
        {
            session.close();
        }
       
        return auto;
    }

}
