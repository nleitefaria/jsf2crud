package com.mycompany.jsf2crud.dao;

public interface LoginDAO {
	
	boolean validate(String username, String password);

}
