package com.mycompany.jsf2crud.dao;

import java.util.List;

import com.mycompany.jsf2crud.entity.Payment;

public interface PaymentDAO {
	
	List<Payment> findAll();
	Payment findOne(String paymentID);

}
