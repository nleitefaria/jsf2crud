package com.mycompany.jsf2crud.dao;

import java.util.List;

import com.mycompany.jsf2crud.entity.Customer;


public interface CustomerDAO 
{	
	List<Customer> findAll();
	Customer findOne(String customerID);
	void save(Customer customer);
	void delete(Integer id);
}
