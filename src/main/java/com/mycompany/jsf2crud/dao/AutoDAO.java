package com.mycompany.jsf2crud.dao;

import java.util.List;

import com.mycompany.jsf2crud.entity.Auto;

public interface AutoDAO 
{	
	List<Auto> findAll();
	public Auto findOne(String autoID);

}
