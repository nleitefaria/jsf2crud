package com.mycompany.jsf2crud.dto;

public class FeatureDTO {
	
	private Short id;
    private String featureName;
    
    public FeatureDTO() 
	{
	}
    
	public FeatureDTO(Short id, String featureName) 
	{
		this.id = id;
		this.featureName = featureName;
	}
	
	public Short getId() 
	{
		return id;
	}
	
	public void setId(Short id)
	{
		this.id = id;
	}
	
	public String getFeatureName() 
	{
		return featureName;
	}
	
	public void setFeatureName(String featureName)
	{
		this.featureName = featureName;
	}
    
    

}
