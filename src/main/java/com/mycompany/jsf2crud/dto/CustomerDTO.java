package com.mycompany.jsf2crud.dto;

import java.util.List;

public class CustomerDTO
{	
	private Integer id;
    private String name;
    private String address;
    private String city;
    private String employmentStatus;
    private String companyName;
    private List<PaymentDTO> paymentsDTO;

    public CustomerDTO()
    {
    }
    
    public CustomerDTO(String name, String employmentStatus) 
    {
        this.name = name;        
        this.employmentStatus = employmentStatus;        
    }

    public CustomerDTO(Integer id, String name, String address, String city, String employmentStatus, String companyName) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.employmentStatus = employmentStatus;
        this.companyName = companyName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

	public List<PaymentDTO> getPaymentsDTO() {
		return paymentsDTO;
	}

	public void setPaymentsDTO(List<PaymentDTO> paymentsDTO) {
		this.paymentsDTO = paymentsDTO;
	}

}
