package com.mycompany.jsf2crud.dto;

public class PaymentDTO {
	
	private Integer id;
    private CustomerDTO customer;
    private String amount;
    
    public PaymentDTO() 
	{	
	}
    
	public PaymentDTO(Integer id, String amount) 
	{	
		this.id = id;
		this.amount = amount;
	}
	
	public PaymentDTO(Integer id, CustomerDTO customer, String amount)
	{
		this.id = id;
		this.customer = customer;
		this.amount = amount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}
}
