package com.mycompany.jsf2crud.dto;

import java.math.BigDecimal;

public class AutoDTO
{	
	private Integer id;
    private String autoModel;
    private short modelYear;
    private BigDecimal askingPrice;
    
    public AutoDTO() 
	{
	}
    
	public AutoDTO(Integer id, String autoModel, short modelYear, BigDecimal askingPrice) 
	{
		this.id = id;
		this.autoModel = autoModel;
		this.modelYear = modelYear;
		this.askingPrice = askingPrice;
	}

	public Integer getId()
	{
		return id;
	}
	
	public void setId(Integer id)
	{
		this.id = id;
	}
	
	public String getAutoModel()
	{
		return autoModel;
	}
	
	public void setAutoModel(String autoModel)
	{
		this.autoModel = autoModel;
	}
	
	public short getModelYear() 
	{
		return modelYear;
	}
	
	public void setModelYear(short modelYear)
	{
		this.modelYear = modelYear;
	}
	
	public BigDecimal getAskingPrice() 
	{
		return askingPrice;
	}
	public void setAskingPrice(BigDecimal askingPrice) 
	{
		this.askingPrice = askingPrice;
	}
}
