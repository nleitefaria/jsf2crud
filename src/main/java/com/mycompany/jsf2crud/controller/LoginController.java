package com.mycompany.jsf2crud.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import com.mycompany.jsf2crud.service.LoginService;


@ManagedBean
@SessionScoped
public class LoginController implements Serializable 
{
	private static final long serialVersionUID = 1L;	
	private static final Logger logger = Logger.getLogger(LoginController.class);

	@Inject 
	LoginService loginService;
	
	private String username;
    private String password;
    private String msg;

    public LoginController() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    
    public void validateUsernamePassword() 
    {   	
    	FacesContext context = FacesContext.getCurrentInstance();
    	boolean valid = loginService.validate(username, password);
       
        if (valid)
        {       	     	
        	Date date = new Date();
        	logger.info("User: " + username + " logged in the system at " + date.toString());
            context.getExternalContext().getSessionMap().put("user", username);
            try 
            {
                context.getExternalContext().redirect("main.xhtml");              
            } 
            catch (IOException e) 
            {
                e.printStackTrace();
            }
        } 
        else 
        { 
            context.addMessage(null, new FacesMessage("Authentication Failed. Check username or password."));
        }
    }

    public void logout()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().invalidateSession();
        try 
        {
        	Date date = new Date();
        	logger.info("User: " + username + " logged off the system at " + date.toString());
            context.getExternalContext().redirect("index.xhtml");
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
}
