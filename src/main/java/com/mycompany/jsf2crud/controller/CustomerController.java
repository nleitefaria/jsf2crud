/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jsf2crud.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.mycompany.jsf2crud.dto.CustomerDTO;
import com.mycompany.jsf2crud.service.CustomerService;

/**
 *
 * @author nleit_000
 */
@ManagedBean
@RequestScoped
public class CustomerController 
{	
	@Inject
	CustomerService customerService;

    private String customerID;  
    private String name;
    private String employmentStatus;
    
    private CustomerDTO customerDTO;
      
    
    public CustomerController()
    {       
        customerID = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerID");          
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public CustomerDTO getCustomerDTO()
	{
    	customerDTO = customerService.findOne(customerID);   	
    	return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }
    
    public String save()
    {
    	customerService.save(new CustomerDTO(name, employmentStatus));
    	return "customers?faces-redirect=true";	
    }

}
