package com.mycompany.jsf2crud.controller;

import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.mycompany.jsf2crud.dto.AutoDTO;
import com.mycompany.jsf2crud.service.AutoService;

@ManagedBean
@RequestScoped
public class AutosController 
{
	@Inject
	AutoService autoService;

    private List<AutoDTO> autoList;
    
    public AutosController()
    {
    }

    public List<AutoDTO> getAutoList()
    { 
    	autoList = autoService.findAll();
        return autoList;
    }

	public void setAutoList(List<AutoDTO> autoList) {
		this.autoList = autoList;
	}
	
	public String autoDetails()
    {    
        Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String autoID = params.get("autoID");
        return "auto?faces-redirect=true&autoID=" + autoID;
    }
    
    
    
    
    
    

}
