package com.mycompany.jsf2crud.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.mycompany.jsf2crud.dto.CustomerDTO;
import com.mycompany.jsf2crud.dto.PaymentDTO;
import com.mycompany.jsf2crud.service.PaymentService;

@ManagedBean
@RequestScoped
public class PaymentController
{
	@Inject
	PaymentService paymentService;

    private String paymentID;
    
    private PaymentDTO paymentDTO;
    
	public PaymentController()
	{
		paymentID = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("paymentID");          		
	}

	public String getPaymentID() {
		return paymentID;
	}

	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}

	public PaymentDTO getPaymentDTO() {
		paymentDTO = paymentService.findOne(paymentID); 
		return paymentDTO;
	}

	public void setPaymentDTO(PaymentDTO paymentDTO) {
		this.paymentDTO = paymentDTO;
	}
	
	

}
