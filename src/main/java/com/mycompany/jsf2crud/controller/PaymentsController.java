package com.mycompany.jsf2crud.controller;

import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.mycompany.jsf2crud.dto.PaymentDTO;
import com.mycompany.jsf2crud.service.PaymentService;

@ManagedBean
@RequestScoped
public class PaymentsController 
{
	@Inject
	PaymentService paymentService;

    private List<PaymentDTO> paymentList;
    
    public PaymentsController()
    {
    }

	public List<PaymentDTO> getPaymentList() 
	{
		paymentList = paymentService.findAll();
		return paymentList;
	}

	public void setPaymentList(List<PaymentDTO> paymentList)
	{
		this.paymentList = paymentList;
	}
	
	public String paymentDetails()
	{
		System.out.println("@paymentDetails");
		Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String paymentID = params.get("paymentID");
        return "payment?faces-redirect=true&paymentID=" + paymentID;
	}
    
    

}
