package com.mycompany.jsf2crud.controller;

import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.mycompany.jsf2crud.dto.CustomerDTO;
import com.mycompany.jsf2crud.service.CustomerService;

@ManagedBean
@RequestScoped
public class CustomersController
{
	@Inject
	CustomerService customerService;

    private List<CustomerDTO> customerList;
    
    public CustomersController()
    {
    }

    public List<CustomerDTO> getCustomerList()
    { 
    	customerList = customerService.findAll();
        return customerList;
    }

    public void setCustomerList(List<CustomerDTO> customerList) {
        this.customerList = customerList;
    }
    
    public String customerDetails()
    {    
        Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String customerID = params.get("customerID");
        return "customer?faces-redirect=true&customerID=" + customerID;
    }
    
    public String create()
    {
    	return "new-customer?faces-redirect=true"; 	
    }
    
    
    public String delete()
    {
    	Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String customerID = params.get("customerID");       
        customerService.delete(customerID);
        return "customers?faces-redirect=true";
    	
    }
    
    
    
    
}
