package com.mycompany.jsf2crud.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.mycompany.jsf2crud.dto.AutoDTO;
import com.mycompany.jsf2crud.service.AutoService;

@ManagedBean
@RequestScoped
public class AutoController
{	
	@Inject
	AutoService autoService;

	private String autoID; 
	private AutoDTO autoDTO;
    
    public AutoController()
    {
    	autoID = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("autoID"); 
    }
 
	public String getAutoID() {
		return autoID;
	}

	public void setAutoID(String autoID) {
		this.autoID = autoID;
	}

	public void setAutoDTO(AutoDTO autoDTO) {
		this.autoDTO = autoDTO;
	}

	public AutoDTO getAutoDTO()
	{
    	autoDTO = autoService.findOne(autoID);   	
    	return autoDTO;
    }

    public void setCustomerDTO(AutoDTO autoDTO) {
        this.autoDTO = autoDTO;
    }


}
