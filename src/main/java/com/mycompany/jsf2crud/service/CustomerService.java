package com.mycompany.jsf2crud.service;

import java.util.List;

import com.mycompany.jsf2crud.dto.CustomerDTO;


public interface CustomerService {
	
	List<CustomerDTO> findAll();
	CustomerDTO findOne(String id);
	void save(CustomerDTO customerDTO);
	void delete(String id);

}
