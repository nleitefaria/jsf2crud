package com.mycompany.jsf2crud.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.mycompany.jsf2crud.dao.AutoDAO;
import com.mycompany.jsf2crud.dto.AutoDTO;
import com.mycompany.jsf2crud.entity.Auto;
import com.mycompany.jsf2crud.service.AutoService;

public class AutoServiceImpl implements AutoService
{
	@Inject
	AutoDAO autoDAO;
	
	public AutoServiceImpl()
	{	
	}
	
	public List<AutoDTO> findAll() 
    {
		List<AutoDTO> autoList = new ArrayList<AutoDTO>();
		for(int i = 0 ; i < autoDAO.findAll().size(); i++)
        {
            Auto a = (Auto) autoDAO.findAll().get(i);           
            autoList.add(new AutoDTO(a.getId(), a.getAutoModel(), a.getModelYear(), a.getAskingPrice()));          
        } 
		return autoList;
    }
	
	public AutoDTO findOne(String id) 
    {		
		Auto a = (Auto) autoDAO.findOne(id); 
		AutoDTO aDTO = new AutoDTO(a.getId(), a.getAutoModel(), a.getModelYear(), a.getAskingPrice());
		return aDTO;
    }

}
