package com.mycompany.jsf2crud.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.mycompany.jsf2crud.dao.FeatureDAO;
import com.mycompany.jsf2crud.dto.FeatureDTO;
import com.mycompany.jsf2crud.entity.Feature;
import com.mycompany.jsf2crud.service.FeatureService;

public class FeatureServiceImpl implements FeatureService
{
	@Inject
	FeatureDAO featureDAO;
	
	public FeatureServiceImpl()
	{	
	}
	
	public List<FeatureDTO> findAll() 
    {
		List<FeatureDTO> featureList = new ArrayList<FeatureDTO>();
		for(int i = 0 ; i < featureDAO.findAll().size(); i++)
        {
			Feature f = (Feature) featureDAO.findAll().get(i);    
			System.out.println("*************");
			System.out.println(f);
			System.out.println("*************");
            featureList.add(new FeatureDTO(f.getId(), f.getFeatureName()));          
        } 
		return featureList;
    }

}
