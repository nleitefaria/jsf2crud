package com.mycompany.jsf2crud.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.mycompany.jsf2crud.dao.PaymentDAO;
import com.mycompany.jsf2crud.dto.CustomerDTO;
import com.mycompany.jsf2crud.dto.PaymentDTO;
import com.mycompany.jsf2crud.entity.Payment;
import com.mycompany.jsf2crud.service.PaymentService;

public class PaymentServiceImpl implements PaymentService
{
	@Inject
	PaymentDAO paymentDAO;
	
	public PaymentServiceImpl()
	{	
	}
	
	public List<PaymentDTO> findAll() 
    {
		List<PaymentDTO> paymentList = new ArrayList<PaymentDTO>();
		CustomerDTO customerDTO = null;
		for(int i = 0 ; i < paymentDAO.findAll().size(); i++)
        {
            Payment p = (Payment) paymentDAO.findAll().get(i);
            customerDTO = new CustomerDTO(p.getCustomer().getId(), p.getCustomer().getName(), p.getCustomer().getAddress(), p.getCustomer().getCity(),p.getCustomer().getEmploymentStatus(), p.getCustomer().getCompanyName());
            paymentList.add(new PaymentDTO(p.getId(), customerDTO ,p.getAmount())); 
        } 
		return paymentList;
    }
	
	public PaymentDTO findOne(String paymentID)
	{
		Payment p = (Payment) paymentDAO.findOne(paymentID);
		CustomerDTO customerDTO = new CustomerDTO(p.getCustomer().getId(), p.getCustomer().getName(), p.getCustomer().getAddress(), p.getCustomer().getCity(),p.getCustomer().getEmploymentStatus(), p.getCustomer().getCompanyName());
        return new PaymentDTO(p.getId(), customerDTO ,p.getAmount());	
	}
	

}
