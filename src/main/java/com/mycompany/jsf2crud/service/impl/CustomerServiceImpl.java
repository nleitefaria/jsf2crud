package com.mycompany.jsf2crud.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.mycompany.jsf2crud.dao.CustomerDAO;
import com.mycompany.jsf2crud.dto.CustomerDTO;
import com.mycompany.jsf2crud.dto.PaymentDTO;
import com.mycompany.jsf2crud.entity.Customer;
import com.mycompany.jsf2crud.entity.Payment;
import com.mycompany.jsf2crud.service.CustomerService;

public class CustomerServiceImpl implements CustomerService 
{	
	@Inject
	CustomerDAO customerDAO;
	
	public CustomerServiceImpl()
	{	
	}
	
	public List<CustomerDTO> findAll() 
    {
		List<CustomerDTO> customerList = new ArrayList<CustomerDTO>();
		for(int i = 0 ; i < customerDAO.findAll().size(); i++)
        {
            Customer c = (Customer) customerDAO.findAll().get(i);           
            customerList.add(new CustomerDTO(c.getId(), c.getName(), c.getAddress(), c.getCity(),  getEmploymentStatus(c.getEmploymentStatus()), c.getCompanyName()));          
        } 
		return customerList;
    }
	
	public CustomerDTO findOne(String id) 
    {
		Customer c = customerDAO.findOne(id);
		CustomerDTO cDTO = new CustomerDTO(c.getId(), c.getName(), c.getAddress(), c.getCity(), c.getEmploymentStatus(), c.getCompanyName());        
		PaymentDTO paymentDTO = null;
		List<PaymentDTO> paymentsDTOAsList = new ArrayList<PaymentDTO>();
		for(Payment p : c.getPayments())
		{
			paymentDTO = new PaymentDTO(p.getId(), p.getAmount());
			paymentsDTOAsList.add(paymentDTO);					
		}
		
		cDTO.setPaymentsDTO(paymentsDTOAsList);
		return cDTO; 
    }
	
	public void save(CustomerDTO customerDTO)
	{
		customerDAO.save(new Customer(customerDTO.getName(), customerDTO.getEmploymentStatus()));
		
	}

	public void delete(String id)
	{
		customerDAO.delete(Integer.parseInt(id));		
	}
	
	private String getEmploymentStatus(String es)
	{
		String employmentStatus = null; 
		if(es != null)
        {
        	if(es.equals("e") == true)
        	{
        		employmentStatus = "Employeed";
        	}
        	else
        	{
        		employmentStatus = "Unemployeed";
        	}          	
        }
        else
        {
        	employmentStatus = "n.a.";
        }
		return employmentStatus;
	}
}
