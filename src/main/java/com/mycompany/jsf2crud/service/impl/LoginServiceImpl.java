package com.mycompany.jsf2crud.service.impl;

import javax.inject.Inject;

import com.mycompany.jsf2crud.dao.LoginDAO;
import com.mycompany.jsf2crud.service.LoginService;

public class LoginServiceImpl implements LoginService{
	
	@Inject
	LoginDAO loginDAO;
	
	public boolean validate(String username, String password)
	{	
		return loginDAO.validate(username, password);
	}

}
