package com.mycompany.jsf2crud.service;

import java.util.List;

import com.mycompany.jsf2crud.dto.FeatureDTO;

public interface FeatureService {
	
	List<FeatureDTO> findAll();

}
