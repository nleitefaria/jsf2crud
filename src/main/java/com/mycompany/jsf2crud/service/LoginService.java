package com.mycompany.jsf2crud.service;

public interface LoginService {
	
	 boolean validate(String username, String password);

}
