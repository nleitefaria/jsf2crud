package com.mycompany.jsf2crud.service;

import java.util.List;

import com.mycompany.jsf2crud.dto.PaymentDTO;

public interface PaymentService
{	
	List<PaymentDTO> findAll();
	PaymentDTO findOne(String paymentID);

}
