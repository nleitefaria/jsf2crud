package com.mycompany.jsf2crud.service;

import java.util.List;

import com.mycompany.jsf2crud.dto.AutoDTO;

public interface AutoService 
{	
	List<AutoDTO> findAll();
	AutoDTO findOne(String id);
}
