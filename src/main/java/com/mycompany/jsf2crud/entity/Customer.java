package com.mycompany.jsf2crud.entity;
// Generated 28/dez/2017 18:33:38 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Customer generated by hbm2java
 */
@Entity
@Table(name="customer"
    ,catalog="jsf2crud"
)
public class Customer  implements java.io.Serializable {


     private Integer id;
     private String name;
     private String address;
     private String city;
     private String employmentStatus;
     private String companyName;
     private Set<Payment> payments = new HashSet<Payment>(0);

    public Customer() {
    }

	
    public Customer(String name, String employmentStatus) {
        this.name = name;
        this.employmentStatus = employmentStatus;
    }
    public Customer(String name, String address, String city, String employmentStatus, String companyName, Set<Payment> payments) {
       this.name = name;
       this.address = address;
       this.city = city;
       this.employmentStatus = employmentStatus;
       this.companyName = companyName;
       this.payments = payments;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    
    @Column(name="name", nullable=false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="address")
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

    
    @Column(name="city")
    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }

    
    @Column(name="employmentStatus", nullable=false, length=2)
    public String getEmploymentStatus() {
        return this.employmentStatus;
    }
    
    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    
    @Column(name="companyName")
    public String getCompanyName() {
        return this.companyName;
    }
    
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="customer")
    public Set<Payment> getPayments() {
        return this.payments;
    }
    
    public void setPayments(Set<Payment> payments) {
        this.payments = payments;
    }




}


